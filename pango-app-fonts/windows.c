#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>

#include <gtk/gtk.h>

static int 
register_font (
    char* font_file
)
{
    return AddFontResourceExA(
      font_file,
      FR_PRIVATE,
      0
    );
}
static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *view;
  GtkTextBuffer *buffer;
  GtkCssProvider *provider;
  GtkStyleContext *context;

  // window intialise
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 500, 500);

  // create text widget
  view = gtk_text_view_new ();
  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
  gtk_text_buffer_set_text (buffer, "Hello World", -1);
  /* Change default font and color throughout the widget */
  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (provider,
                                  "textview {"
                                  " font: 60px Dancing Script;"
                                  "}",
                                  -1,
                                  NULL);
  context = gtk_widget_get_style_context (view);
  gtk_style_context_add_provider (context,
                                  GTK_STYLE_PROVIDER (provider),
                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  /* Change left margin throughout the widget */
  gtk_text_view_set_left_margin (GTK_TEXT_VIEW (view), 100);

  // show it
  gtk_container_add (GTK_CONTAINER (window), view);

  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  // do before anything
  printf("%d fonts registered.",register_font("font/DancingScript-VariableFont_wght.ttf"));
  GtkApplication *app;
  int status;

  app = gtk_application_new ("website.syrusdark.fonts", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
