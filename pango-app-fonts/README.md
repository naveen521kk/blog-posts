# Pango Application Fonts

Blog Post: https://blog.syrusdark.website/2021/04/how-to-use-custom-application-fonts.html

## Windows

The *demo* program can be compiled with GCC using

```sh
gcc windows.c `pkg-config --cflags --libs gtk+-3.0`
```

